package com.mastertech.acesso.consumer;

import com.mastertech.acesso.producer.Acesso;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CSVUtil {

    public static String convertToCSV(Acesso data) {
        String [] array = new String[3];
        array[0] = data.getCliente();
        array[1] = data.getPorta();
        array[2] = String.valueOf(data.getTemAcesso());

        return Stream.of(array)
                .collect(Collectors.joining(","));
    }

    public static void gravaNoCSV(String data) throws IOException {
        System.out.println("Inicio gravacao");
        String fileName = "/home/a2/Documentos/csvfiles/acessos.csv";
        File file = new File(fileName);

        if (!file.exists()) {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            writer.write(data);
            writer.newLine();
            writer.close();
        } else {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.append(data);
            writer.newLine();
            writer.close();
        }
        System.out.println("Fim gravacao");
    }
}
