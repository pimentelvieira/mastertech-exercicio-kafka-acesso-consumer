package com.mastertech.acesso.consumer;

import com.mastertech.acesso.producer.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec2-william-pimentel-1", groupId = "wilpime")
    public void receber(@Payload Acesso acesso) throws IOException {
        System.out.println("Inicio consumer");
        CSVUtil.gravaNoCSV(CSVUtil.convertToCSV(acesso));
        System.out.println("Fim consumer");
    }
}
